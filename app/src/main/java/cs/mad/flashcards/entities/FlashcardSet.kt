package cs.mad.flashcards.entities

//The Flashcard class should have term and definition properties
//  Inside this class, write a function that returns a hardcoded collection of 10 Flashcards
//The FlashcardSet class should have a title.
//  Inside this class, write a function that returns a hardcoded collection of 10 FlashcardSets

data class FlashcardSet(val title: String){
    fun exCardSets(): MutableList<FlashcardSet> {
        var hardTitles = arrayOf(
            "first",
            "second",
            "third",
            "fourth",
            "fifth",
            "sixth",
            "seventh",
            "eighth",
            "ninth",
            "tenth"
        )

        val deckSize = hardTitles.size
        var CardSets = mutableListOf<FlashcardSet>()

        for (t in 0..deckSize) {
            CardSets.add(FlashcardSet(hardTitles.get(t)))
        }

        return CardSets;
    }
}
