package cs.mad.flashcards.entities

//The Flashcard class should have term and definition properties
//  Inside this class, write a function that returns a hardcoded collection of 10 Flashcards
//The FlashcardSet class should have a title.
//  Inside this class, write a function that returns a hardcoded collection of 10 FlashcardSets

data class Flashcard(val term: String, val definition: String){
    fun exCards(): MutableList<Flashcard> {
        var hardTerms = arrayOf(
            "cherry",
            "banana",
            "lime",
            "tangerine",
            "eggplant",
            "ricotta",
            "grandma's goop",
            "blueberry",
            "apple",
            "pork"
        )
        var hardDefs = arrayOf(
            "red",
            "yellow",
            "green",
            "orange",
            "purple",
            "white",
            "brown",
            "blue",
            "green :)",
            "pink"
        )

        val deckSize = hardTerms.size
        var Cards = mutableListOf<Flashcard>()

        for (t in 0..deckSize) {
            Cards.add(Flashcard(hardTerms.get(t), hardDefs.get(t)))
        }

        return Cards;
    }
}
